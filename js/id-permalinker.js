$j=jQuery.noConflict();

// Use jQuery via $j(...)
$j(document).ready(function() {
	var clip = null;
	// Enable Rich HTML support (Flash Player 10 Only)
	//ZeroClipboard.setMoviePath( '/wp-content/plugins/id-permalinker/js/ZeroClipboard10.swf' );
	ZeroClipboard.setMoviePath( '/wp-content/plugins/id-permalinker/js/ZeroClipboard.swf' );

	$j('div.idp_id').mouseover(function() {
		// Set up a single ZeroClipboard object for all elements
		clip = new ZeroClipboard.Client();
		clip.setHandCursor( true );

		clip.setText( '[permalink id=' + this.innerHTML + ']Your link text here[/permalink]');
		//clip.glue(this);
		// reposition the movie over our element
		// or create it if this is the first time
		if (clip.div) {
			clip.receiveEvent('mouseout', null);
			clip.reposition(this);
		}
		else clip.glue(this);

		// gotta force these events due to the Flash movie
		// moving all around.  This insures the CSS effects
		// are properly updated.
		clip.receiveEvent('mouseover', null);

		clip.addEventListener('mouseover', function(client) {
			$j('#idp_notify').css("background-color","red").text('Click to copy the permalink for this ID to your clipboard.').fadeIn("slow");
		});

		clip.addEventListener('mouseout', function(client) {
			$j('#idp_notify').fadeOut("slow");
		});

		clip.addEventListener('complete', function(client, text) {
			//$j('#idp_notify').fadeOut("slow");
			//$j('#idp_notify').css("background-color","green").text('"' + text + '" copied to your clipboard!').fadeIn("slow").fadeOut("slow");
			$j('#idp_notify').css("background-color","green").text('"' + text + '" copied to your clipboard!').fadeOut(5000);
		});
	});
});
