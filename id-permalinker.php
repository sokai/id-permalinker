<?php
/*
Plugin Name: ID-Permalinker
Plugin URI: http://
Description: Simply shows the ID of Posts, Pages, Media, Links, Categories, Tags and Users in the admin tables for easy access. Very lightweight. Combined with The Permalinker Plugin.
Author: Kai Sommer
Author URI: http://
Version: 1.0

	Copyright (c) 2009-2010 Matt Martz (http://sivel.net)
	Simply Show IDs is released under the GNU General Public License (GPL)
	http://www.gnu.org/licenses/gpl-2.0.txt
*/

// Prepend the new column to the columns array
function idpermalinker_column($cols) {
	echo "<div id=\"idp_notify\"></div>";
	$cols['idpermalinker_id'] = 'ID';
	return $cols;
}

// Echo the ID for the new column
function idpermalinker_value($column_name, $id) {
	if ($column_name == 'idpermalinker_id')
		echo "<div class=\"idp_id\">".$id."</div>";
}

function idpermalinker_return_value($value, $column_name, $id) {
	if ($column_name == 'idpermalinker_id')
		$value = $id;
	return $value;
}

// Output CSS for width of new column
function idpermalinker_css() {
?>
<!-- Simply Show IDs -->
<style type="text/css">
	#idpermalinker_id { width: 50px; }
	/*#idpermalinker_notify { position: fixed; background: black; color: white;  padding: 5px; display: none; opacity: 0.8; z-index: 2; }*/
	div.idp_id { width: 100%; height: 40px; }
	div#idp_notify { position: fixed; background: black; color: white;  padding: 5px; display: none; z-index: 99; }
</style>
<?php
}

// Example:
// [permalink id=123]My 123rd post![/permalink]
//
function idpermalinker_links($atts, $content = null) {
	extract(shortcode_atts(array(
		'id' => null,
		'target' => null,
		'class' => null,
		'rel' => null
	), $atts));
	if ( empty($id) ) {
		$id = get_the_ID();
	}
	$content = trim($content);
	if ( !empty($content) ) {
		$output = '<a href="' . get_permalink($id) . '"';
		if ( !empty($target) ) {
			$output .= ' target="' . $target . '"';
		}
		$output .= ' class="idpermalinker_link';
		if ( !empty($class) ) {
			$output .= " $class";
		}
		$output .= '"';
		if ( !empty($rel) ) {
			$output .= ' rel="' . $rel . '"';
		}
		$output .= '>' . $content . '</a>';
	}
	else {
		$output = get_permalink($id);
	}
	return $output;
}

// Example:
// <img src="[template_uri]/images/my_inline_image.jpg" alt="Photo" />
//
function idpermalinker_template_uri( $atts, $content = null ) {
	return get_template_directory_uri();
}

function idpermalinker_help() {
?>
<div class="wrap" style="width:80%;">
	<h2>Help With The Permalinker</h2>
	<p><strong>The Basics</strong></p>
	<p>Inserting a permalink is easy using the <code>[permalink]</code> short code:</p>
	<pre><code>[permalink]This is a link to the current post[/permalink]</code></pre>
	<p>If you'd like create a permalink to a different post than the one being displayed,
	use the ID attribute:</p>
	<pre><code>[permalink id=23]This is a link to post 23[/permalink]</code></pre>

	<p><strong>Anchor Element Attributes</strong></p>
	<p>Some users may want to add a CSS <code>class</code>, relationship (<code>rel</code>),
	or even a <code>target</code> attribute to the links that The Permalinker outputs.</p>
	<p>The latter 3 attributes are supported in your <code>[permalink]</code> short code:</p>
	<pre><code>[permalink id=23 rel="post" class="my_class" target="_blank"]Open post 23 in a new window[/permalink]</code></pre>

	<p><strong>Other Permalinker Notes</strong></p>
	<p>Just a few quick final notes:</p>
	<ul style="list-style:disc;margin: 1em 0 1em 3em;">
		<li>Leading and trailing whitespace is ignored.</li>
		<li>The class <code>permalinker_link</code> is always applied to a link generated from our short code.
		<li>
			If you are experiencing unexpected results when using terminating (<code>[permalink]anchor text[/permalink]</code>) and non-terminating
			(<code>[permalink]</code>) permalink short codes, change all non-terminating short codes to terminating short codes containing
			only whitespace:<br /><br /><code>[permalink] [/permalink]</code>
		</li>
	</ul>
	<p>If you need more support, email Andy <a href="mailto:hello@theandystratton.com">hello@theandystratton.com</a>
	or visit his blog at <a href="http://theandystratton.com/blog/" target="_blank">theandystratton.com</a>

	<p><strong>Template URI Short Code</strong></p>
	<p>The <code>[template_uri]</code> short code was added for designers/developers that
	want to easily get their current theme's directory URI when linking to resources that
	exist in their theme directory, such as stock photos, flash movies, etc.</p>
	<p><em>Example of Template URI usage:</em></p>
	<pre><code>&lt;img src="[template_uri]/photos/yoda.jpg" alt="A photo of Yoda." /&gt;</code></pre>

</div>
<?php
}

function idpermalinker_admin_menu() {
	add_submenu_page('options-general.php', 'ID-Permalinker Help', 'ID-Permalinker Help', 1, 'idpermalinker_help', 'idpermalinker_help');
}

// i18n plugin domain
define('ID_PERMALINKER_I18N_DOMAIN', 'id-permalinker');

/**
 * Initialise the internationalisation domain
 */
load_plugin_textdomain(ID_PERMALINKER_I18N_DOMAIN,
			'wp-content/plugins/id-permalinker/languages','id-permalinker/languages');

// Actions/Filters for various tables and the css output
function idpermalinker_add() {
	global $wp_taxonomies;
	add_action('admin_head', 'idpermalinker_css');
	wp_enqueue_script('zeroclipboardjs', plugins_url('js/ZeroClipboard.js',__FILE__), array('jquery') );
	wp_enqueue_script('idpermalinkerjs', plugins_url('js/id-permalinker.js',__FILE__) );

	add_filter('manage_posts_columns', 'idpermalinker_column');
	add_action('manage_posts_custom_column', 'idpermalinker_value', 10, 2);

	add_filter('manage_pages_columns', 'idpermalinker_column');
	add_action('manage_pages_custom_column', 'idpermalinker_value', 10, 2);

	add_filter('manage_media_columns', 'idpermalinker_column');
	add_action('manage_media_custom_column', 'idpermalinker_value', 10, 2);

	add_filter('manage_link-manager_columns', 'idpermalinker_column');
	add_action('manage_link_custom_column', 'idpermalinker_value', 10, 2);

	add_action('manage_edit-link-categories_columns', 'idpermalinker_column');
	add_filter('manage_link_categories_custom_column', 'idpermalinker_return_value', 10, 3);

	add_action('manage_edit-tags_columns', 'idpermalinker_column');
	add_action('manage_categories_columns', 'idpermalinker_column');
	foreach ( $wp_taxonomies as $taxonomy => $object )
		add_filter("manage_${taxonomy}_custom_column", 'idpermalinker_return_value', 10, 3);

	// Hack for WordPress version prior to 3.0, where the taxonomy was referenced as categories
	add_filter('manage_categories_custom_column', 'idpermalinker_return_value', 10, 3);

	add_action('manage_users_columns', 'idpermalinker_column');
	add_filter('manage_users_custom_column', 'idpermalinker_return_value', 10, 3);

	add_action('manage_edit-comments_columns', 'idpermalinker_column');
	add_action('manage_comments_custom_column', 'idpermalinker_value', 10, 2);
}

add_action('admin_init', 'idpermalinker_add');

// from The Permalinker
add_action('admin_menu', 'idpermalinker_admin_menu');
add_shortcode('permalink', 'idpermalinker_links');
add_shortcode('template_uri', 'idpermalinker_template_uri');
